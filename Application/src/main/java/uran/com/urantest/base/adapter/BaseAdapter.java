package uran.com.urantest.base.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseAdapter<T> extends RecyclerView.Adapter<BaseViewHolder<T>> {

    private List<T> listElements = new ArrayList<>();

    public void addItems(List<T> items) {
        listElements.addAll(items);
        notifyDataSetChanged();
    }

    protected T getItem(int position) {
        return listElements.get(position);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<T> baseViewHolder, int i) {
        T item = getItem(i);
        if (item != null)
            baseViewHolder.bindData(item);
    }

    @Override
    public int getItemCount() {
        return listElements.size();
    }
}
