package uran.com.urantest.base;

public interface BasePresenter<T> {
    void bindView(T view);

    void unbindView();
}
