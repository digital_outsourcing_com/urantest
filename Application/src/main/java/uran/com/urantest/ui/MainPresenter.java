package uran.com.urantest.ui;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;
import uran.com.fileexhibitsloader.FileExhibitsLoader;
import uran.com.model.Exhibit;
import uran.com.model.ExhibitsLoader;

public class MainPresenter implements MainContract.Presenter {

    private MainContract.View mainView;

    private ExhibitsLoader<Exhibit> exhibitsLoader;

    MainPresenter(FileExhibitsLoader fileExhibitsLoader) {
        this.exhibitsLoader = fileExhibitsLoader;
    }

    @Override
    public void bindView(MainContract.View view) {
        this.mainView = view;
    }

    @Override
    public void unbindView() {

    }

    @Override
    public void loadExhibits() {
        exhibitsLoader.getExhibitList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Exhibit>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<Exhibit> exhibits) {
                        if (mainView != null)
                            mainView.setExhibits(exhibits);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e);
                    }
                });
    }
}
