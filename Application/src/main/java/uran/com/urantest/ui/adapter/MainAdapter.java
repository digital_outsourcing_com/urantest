package uran.com.urantest.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uran.com.model.Exhibit;
import uran.com.urantest.R;
import uran.com.urantest.base.adapter.BaseAdapter;
import uran.com.urantest.base.adapter.BaseViewHolder;

public class MainAdapter extends BaseAdapter<Exhibit> {
    @NonNull
    @Override
    public MainItem onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_main, viewGroup, false);
        return new MainItem(v);
    }

    class MainItem extends BaseViewHolder<Exhibit> {
        private AppCompatTextView tvTitle;
        private RecyclerView rvImages;
        private ImagesAdapter imagesAdapter;

        MainItem(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            rvImages = itemView.findViewById(R.id.rv_images);
            rvImages.setLayoutManager(new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false));
            imagesAdapter = new ImagesAdapter();
            rvImages.setAdapter(imagesAdapter);
        }

        @Override
        protected void bindData(Exhibit exhibit) {
            tvTitle.setText(exhibit.getTitle());
            imagesAdapter.addItems(exhibit.getImages());
        }
    }
}
