package uran.com.urantest.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import uran.com.urantest.R;
import uran.com.urantest.base.adapter.BaseAdapter;
import uran.com.urantest.base.adapter.BaseViewHolder;

public class ImagesAdapter extends BaseAdapter<String> {
    @NonNull
    @Override
    public ImagesAdapter.ImagesItem onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_images, viewGroup, false);
        return new ImagesAdapter.ImagesItem(v);
    }

    class ImagesItem extends BaseViewHolder<String> {

        private AppCompatImageView ivLogo;

        ImagesItem(@NonNull View itemView) {
            super(itemView);
            ivLogo = itemView.findViewById(R.id.iv_logo);
        }

        @Override
        protected void bindData(String url) {
            Glide.with(itemView.getContext()).load(url).into(ivLogo);
        }
    }
}
