package uran.com.urantest.ui;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import uran.com.fileexhibitsloader.FileExhibitsLoader;
import uran.com.model.Exhibit;
import uran.com.urantest.AssetsUtil;
import uran.com.urantest.R;
import uran.com.urantest.ui.adapter.MainAdapter;

public class MainFragment extends Fragment implements MainContract.View {

    private MainPresenter mainPresenter;

    private MainAdapter mainAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainPresenter = new MainPresenter(new FileExhibitsLoader(AssetsUtil.loadJsonFromAsset(getContext(), "data.json")));
        mainPresenter.bindView(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        RecyclerView rvList = view.findViewById(R.id.rv_list);
        rvList.setLayoutManager(new LinearLayoutManager(getContext()));
        mainAdapter = new MainAdapter();
        rvList.setAdapter(mainAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        mainPresenter.loadExhibits();
    }

    @Override
    public void setExhibits(List<Exhibit> exhibits) {
        mainAdapter.addItems(exhibits);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mainPresenter.unbindView();
    }
}
