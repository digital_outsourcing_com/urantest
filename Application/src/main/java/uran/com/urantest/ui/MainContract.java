package uran.com.urantest.ui;

import java.util.List;

import uran.com.model.Exhibit;
import uran.com.urantest.base.BasePresenter;
import uran.com.urantest.base.BaseView;

public interface MainContract {
    interface View extends BaseView<Presenter> {
        void setExhibits(List<Exhibit> exhibits);
    }

    interface Presenter extends BasePresenter<View> {
        void loadExhibits();
    }
}
