package uran.com.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Exhibit implements Parcelable {
    private String title;
    private List<String> images;

    protected Exhibit(Parcel in) {
        title = in.readString();
        images = in.createStringArrayList();
    }

    public static final Creator<Exhibit> CREATOR = new Creator<Exhibit>() {
        @Override
        public Exhibit createFromParcel(Parcel in) {
            return new Exhibit(in);
        }

        @Override
        public Exhibit[] newArray(int size) {
            return new Exhibit[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeStringList(images);
    }
}
