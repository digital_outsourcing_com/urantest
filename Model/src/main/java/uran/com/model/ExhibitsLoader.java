package uran.com.model;

import java.util.List;

import io.reactivex.Single;

public interface ExhibitsLoader<T> {
    Single<List<T>> getExhibitList();
}
