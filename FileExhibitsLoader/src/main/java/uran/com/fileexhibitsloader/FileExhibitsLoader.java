package uran.com.fileexhibitsloader;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;
import uran.com.model.Exhibit;
import uran.com.model.ExhibitsLoader;

public class FileExhibitsLoader implements ExhibitsLoader<Exhibit> {

    private String mData;

    public FileExhibitsLoader(String data) {
        this.mData = data;
    }

    @Override
    public Single<List<Exhibit>> getExhibitList() {
        return Single.fromCallable(new Callable<List<Exhibit>>() {
            @Override
            public List<Exhibit> call() throws Exception {
                Gson gson = new Gson();
                JSONObject jsonObject = new JSONObject(mData);
                JSONArray jsonArray = jsonObject.getJSONArray("list");
                List<Exhibit> exhibits = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject exhibitObject = jsonArray.getJSONObject(i);
                    Exhibit exhibit = gson.fromJson(exhibitObject.toString(), Exhibit.class);
                    exhibits.add(exhibit);
                }
                return exhibits;
            }
        });
    }
}
